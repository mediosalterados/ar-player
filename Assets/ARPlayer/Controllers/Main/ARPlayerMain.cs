﻿using UnityEngine;
using System.Collections;

using GLIB.Extended;
using GLIB.Utils;
using GLIB.Audio;
using GLIB.VFX;

using Vuforia;

namespace ARPlayer.Controllers.Main {

	public class ARMediaCocaMain : MonoBehaviour {

		// Use this for initialization
		void Start() {
			SoundModule.Instance.Initialize();
			ParticlesManager.Instance.Initialize();

		}

		// Update is called once per frame
		void Update() {



		}
	}

}