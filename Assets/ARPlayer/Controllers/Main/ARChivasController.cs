﻿using UnityEngine;
using System.Collections;
using System;

namespace ARPlayer.Controllers.Main
{
	
	public class ARChivasController : MonoBehaviour {
		
		public GameObject stadium;
		public GameObject chicharo;
		
		bool doReset = false;

		// Use this for initialization
		void Start () {



		}
		
		// Update is called once per frame
		void Update () {
			
			bool tracking = FindObjectOfType<ARPlayerTrackableEventHandler>().IsTrackableVisible;
			
			if(tracking){
				
				
				if(doReset)
				{
					Reset();
					doReset = false;
				}
				
				if(stadium != null)
				{
					if(!stadium.GetComponent<Animation>().IsPlaying("Intro"))
					{
						stadium.GetComponent<Animation>().Play("Loop");
						chicharo.SetActive (true);

					}


				}

								
				if(!GetComponent<AudioSource>().isPlaying)
					GetComponent<AudioSource>().Play();
			}
			else 
			{
				if(!doReset)
				{
					RewindAnimations();
					doReset = true;
					chicharo.SetActive (false);
				}
				

				
				if(GetComponent<AudioSource>().isPlaying)
					GetComponent<AudioSource>().Stop();
			}
			
		}
		
		public void RewindAnimations(){
			
			try{
				
				stadium.GetComponent<Animation>().Rewind("Intro");
				stadium.GetComponent<Animation>().Rewind("Loop");
				
			}
			catch (NullReferenceException e)
			{
				Debug.LogError("Prefab not Found!: " + e.Message);
				return;
			}
			
		}
		
		
		void Reset()
		{
			try{
				stadium.GetComponent<Animation>().Rewind("Intro");
				stadium.GetComponent<Animation>().Play("Intro");
				
			}
			catch(NullReferenceException e)
			{
				Debug.LogError("Prefab not Found!: " + e.Message);
				return;
			}
		}
	}
	
}