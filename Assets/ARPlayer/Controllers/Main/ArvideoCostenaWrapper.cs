﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Vuforia;
using ARPlayer.Controllers.Main;
public class ArvideoCostenaWrapper : MonoBehaviour {

	GameObject _mainHome;
	GameObject _video;
	GameObject _bg;
	bool enabled_gui = false;


	void Start () {
		
		_mainHome = this.Find("Play<Img>",this.gameObject);
		_video = this.Find("VideoChef<Wrapper>",this.gameObject);
		_bg = this.Find("Bg<Img>",this.gameObject);

		Button videoButton = this.FindAndResolveComponent<Button>("Play<Button>",this.gameObject);
		videoButton.onClick.AddListener (delegate {_mainHome.SetActive(false);_bg.SetActive(false);_video.SetActive(true);});

		Button videoBackButton = this.FindAndResolveComponent<Button>("Back<Button>",this.gameObject);
		videoBackButton.onClick.AddListener (delegate {Debug.Log("Coas");_mainHome.SetActive(true);_bg.SetActive(true);_video.SetActive(false);});
	
		_video.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		
		ARPlayerTrackableEventHandler[] handlers = FindObjectsOfType<ARPlayerTrackableEventHandler> ();

		bool isTracking = false;

		foreach (ARPlayerTrackableEventHandler handler in handlers) {
			if (handler.IsTrackableVisible) {
				isTracking = true;
				break;
			}

		}

		if (isTracking == true && enabled_gui == false) {
			enabled_gui = true;
			_mainHome.SetActive (true);
			_bg.SetActive (true);
			_video.SetActive (false);

		
		} else if (isTracking == false && enabled_gui == true) {
			_mainHome.SetActive (false);
			_bg.SetActive (false);
			_video.SetActive (false);
			enabled_gui = false;
		} else if (isTracking == false && enabled_gui == false) {
			_mainHome.SetActive (false);
			_video.SetActive (false);
			_bg.SetActive (false);
		}
	}
}
