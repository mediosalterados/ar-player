﻿using UnityEngine;
using Vuforia;



namespace ARPlayer.Controllers.Main {
	/// <summary>
	/// A AppTVYNRM custom handler that implements the ITrackableEventHandler interface, and handles Canvas in addition of renderers
	/// </summary>
	public class ARPlayerTrackableEventHandler : MonoBehaviour,
	ITrackableEventHandler
	{
		#region PRIVATE_MEMBER_VARIABLES

		private TrackableBehaviour mTrackableBehaviour;

		#endregion // PRIVATE_MEMBER_VARIABLES

		bool _trackableVisible = false;
		public bool IsTrackableVisible {get{return _trackableVisible;} set{_trackableVisible = value;} } 

		#region UNTIY_MONOBEHAVIOUR_METHODS

		void Start()
		{
			mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			if (mTrackableBehaviour)
			{
				mTrackableBehaviour.RegisterTrackableEventHandler(this);
			}
		}

		#endregion // UNTIY_MONOBEHAVIOUR_METHODS


		#region PUBLIC_METHODS

		/// <summary>
		/// Implementation of the ITrackableEventHandler function called when the
		/// tracking state changes.
		/// </summary>
		public void OnTrackableStateChanged(
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus)
		{
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
				newStatus == TrackableBehaviour.Status.TRACKED ||
				newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
			{
				OnTrackingFound();
			}
			else
			{
				OnTrackingLost();
			}
		}

		#endregion // PUBLIC_METHODS



		#region PRIVATE_METHODS


		private void OnTrackingFound()
		{
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
			Canvas[] canvasComponents = GetComponentsInChildren<Canvas> (true);

			// Enable rendering:
			foreach (Renderer component in rendererComponents)
			{
				component.enabled = true;
			}

			// Enable colliders:
			foreach (Collider component in colliderComponents)
			{
				component.enabled = true;
			}

			foreach (Canvas component in canvasComponents)
				component.enabled = true;

			// Check if Media player is playing this child's Media Wrapper


			Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

			_trackableVisible = true;
		}


		private void OnTrackingLost()
		{
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
			Canvas[] canvasComponents = GetComponentsInChildren<Canvas> (true);

			// Disable rendering:
			foreach (Renderer component in rendererComponents)
			{
				component.enabled = false;
			}

			// Disable colliders:
			foreach (Collider component in colliderComponents)
			{
				component.enabled = false;
			}

			foreach (Canvas component in canvasComponents)
				component.enabled = false;


			// Check if Media player is playing this child's Media Wrapper

			Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");

			_trackableVisible = false;
		}

		#endregion // PRIVATE_METHODS
	}
}
