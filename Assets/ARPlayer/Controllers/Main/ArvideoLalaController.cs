﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Vuforia;
using ARPlayer.Controllers.Main;
public class ArvideoLalaController : MonoBehaviour {


	GameObject _t1;
	GameObject _t2;
	GameObject _t3;

	bool enabled_gui = false;


	void Start () {


		_t1 = this.Find("ToolTip2<Image>",this.gameObject);
		_t2 = this.Find("ToolTip1<Image>",this.gameObject);
		_t3 = this.Find("ToolTip3<Image>",this.gameObject);
	


		Button button1Button = this.FindAndResolveComponent<Button>("Tip1<Button>",this.gameObject);
		button1Button.onClick.AddListener (delegate {_t1.SetActive(true);_t2.SetActive(false);_t3.SetActive(false);});

		Button button2Button = this.FindAndResolveComponent<Button>("Tip2<Button>",this.gameObject);
		button2Button.onClick.AddListener (delegate {_t1.SetActive(false);_t2.SetActive(true);_t3.SetActive(false);});

		Button button3Button = this.FindAndResolveComponent<Button>("Tip3<Button>",this.gameObject);
		button3Button.onClick.AddListener (delegate {_t1.SetActive(false);_t2.SetActive(false);_t3.SetActive(true);});



		_t1.SetActive (false);
		_t2.SetActive (false);
		_t3.SetActive (false);


	}

	// Update is called once per frame
	void Update () {

		ARPlayerTrackableEventHandler[] handlers = FindObjectsOfType<ARPlayerTrackableEventHandler> ();

		bool isTracking = false;

		foreach (ARPlayerTrackableEventHandler handler in handlers) {
			if (handler.IsTrackableVisible) {
				isTracking = true;
				break;
			}

		}

		if (isTracking == true && enabled_gui == false) {
			enabled_gui = true;
		} else if (isTracking == false && enabled_gui == true) {
			_t1.SetActive (false);
			_t2.SetActive (false);
			_t3.SetActive (false);
			enabled_gui = false;
		} 
	}
}
