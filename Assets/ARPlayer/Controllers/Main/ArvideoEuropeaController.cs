﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Vuforia;
using ARPlayer.Controllers.Main;
public class ArvideoEuropeaController : MonoBehaviour {

	GameObject _mainContent;
	GameObject _video;

	bool enabled_gui = false;


	void Start () {
		

		_mainContent = this.Find("MainContent<Image>",this.gameObject);
		_video = this.Find("MainContentVideo<Wrapper>",this.gameObject);


		Button leftButton = this.FindAndResolveComponent<Button>("Left<Button>",this.gameObject);
		leftButton.onClick.AddListener (delegate {_mainContent.SetActive(true);_video.SetActive(false);});

		Button rightButton = this.FindAndResolveComponent<Button>("Right<Button>",this.gameObject);
		rightButton.onClick.AddListener (delegate {_mainContent.SetActive(false);_video.SetActive(true);});
	
		Button c1Button = this.FindAndResolveComponent<Button>("Close<Button>",_mainContent);
		c1Button.onClick.AddListener (delegate {_mainContent.SetActive(false);});

		Button c2Button = this.FindAndResolveComponent<Button>("Close<Button>",_video);
		c2Button.onClick.AddListener (delegate {_video.SetActive(false);});


		_mainContent.SetActive (false);
		_video.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		
		ARPlayerTrackableEventHandler[] handlers = FindObjectsOfType<ARPlayerTrackableEventHandler> ();

		bool isTracking = false;

		foreach (ARPlayerTrackableEventHandler handler in handlers) {
			if (handler.IsTrackableVisible) {
				isTracking = true;
				break;
			}

		}

		if (isTracking == true && enabled_gui == false) {
			enabled_gui = true;
		} else if (isTracking == false && enabled_gui == true) {
			_mainContent.SetActive (false);
			_video.SetActive (false);
			enabled_gui = false;
		} 
	}
}
