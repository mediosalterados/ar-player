﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Vuforia;
using ARPlayer.Controllers.Main;
public class ArvideoWrapper : MonoBehaviour {

	GameObject _baseContent;
	Button _playButton;
	GameObject _topImage;
	Button _backButton;
	Button _fullScreenButton;
	MediaPlayerCtrl _videoPlayer;

	bool enabled_gui = false;



	void Start () {
		
		_baseContent = this.Find("BaseContent<Image>",this.gameObject);
		_playButton = this.FindAndResolveComponent<Button>("Play<Button>",this.gameObject);
		_topImage = this.Find("TopVideo<Image>",this.gameObject);
		_backButton = this.FindAndResolveComponent<Button>("Back<Button>",this.gameObject);
		_fullScreenButton = this.FindAndResolveComponent<Button>("FullScreen<Button>",this.gameObject);
		_videoPlayer = this.FindAndResolveComponent<MediaPlayerCtrl> ("VideoFeed<RawImage>",this.gameObject);


		_playButton.onClick.AddListener (delegate {_baseContent.SetActive(false);_topImage.SetActive(true);_playButton.gameObject.SetActive(false);_videoPlayer.gameObject.SetActive(true);_backButton.gameObject.SetActive(true);_fullScreenButton.gameObject.SetActive(true);});
		_backButton.onClick.AddListener (delegate {_topImage.SetActive(false);_baseContent.SetActive(true);_playButton.gameObject.SetActive(true);_videoPlayer.gameObject.SetActive(false);_backButton.gameObject.SetActive(false);_fullScreenButton.gameObject.SetActive(false);});


		_fullScreenButton.onClick.AddListener (delegate() {
			GameObject videoFeed = this.Find("VideoFeed<RawImage>",this.gameObject);
			FullScreenWidget.Instance.videoFeed = videoFeed;
			FullScreenWidget.Instance.Initialize();
		});

		_fullScreenButton.gameObject.SetActive (false);
		_topImage.SetActive (false);
		_backButton.gameObject.SetActive (false);
		_videoPlayer.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
		ARPlayerTrackableEventHandler[] handlers = FindObjectsOfType<ARPlayerTrackableEventHandler> ();

		bool isTracking = false;

		foreach (ARPlayerTrackableEventHandler handler in handlers) {
			if (handler.IsTrackableVisible) {
				isTracking = true;
				break;
			}

		}

		if (isTracking == true && enabled_gui == false) {
			enabled_gui = true;
			_baseContent.SetActive(true);_playButton.gameObject.SetActive(true);_videoPlayer.gameObject.SetActive(false);_backButton.gameObject.SetActive(false);_fullScreenButton.gameObject.SetActive (false);_topImage.SetActive(false);
		
		} else if (isTracking == false && enabled_gui == true) {
			_baseContent.SetActive(false);_playButton.gameObject.SetActive(false);_videoPlayer.gameObject.SetActive(false);_backButton.gameObject.SetActive(false);_fullScreenButton.gameObject.SetActive(false);_topImage.SetActive(false);
			enabled_gui = false;

			if(FullScreenWidget.Instance.isRunning){
				FullScreenWidget.Instance.Terminate ();
			}
		}
			
	}




}
