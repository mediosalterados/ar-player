﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using System;
using Vuforia;
using ARPlayer.Controllers.Main;
public class ARVideoJLController : MonoBehaviour {


	GameObject _video;

	bool enabled_gui = false;


	void Start () {
		
		_video = this.Find("MainVideo<Wrapper>",this.gameObject);


		Button videoButton = this.FindAndResolveComponent<Button>("Play<Button>",this.gameObject);
		videoButton.onClick.AddListener (delegate {_video.SetActive(true);});

		_video.SetActive (false);

	}

	// Update is called once per frame
	void Update () {

		ARPlayerTrackableEventHandler[] handlers = FindObjectsOfType<ARPlayerTrackableEventHandler> ();

		bool isTracking = false;

		foreach (ARPlayerTrackableEventHandler handler in handlers) {
			if (handler.IsTrackableVisible) {
				isTracking = true;
				break;
			}

		}

		if (isTracking == true && enabled_gui == false) {
			enabled_gui = true;
			_video.SetActive (false);
		} else if (isTracking == false && enabled_gui == true) {
			_video.SetActive (false);
			enabled_gui = false;
		} else if (isTracking == false && enabled_gui == false) {
			_video.SetActive (false);
		}
	}
}
