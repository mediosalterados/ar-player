﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

using GLIB.Extended;
using GLIB.Audio;
using GLIB.VFX;

using ARMediaCoca.Views.Main.Screens;
using ARMediaCoca.Views.Main.Widgets;
using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Controllers.EventHandlers;
using ARMediaCoca.Models.Interfaces;
using ARPlayer.Controllers.Main;

namespace ARMediaCoca.Controllers.AR
{

    public class CokeStage : MonoBehaviour, IStage
    {

        Camera _mainCamera;

        #region MENU CAPS
        GameObject _capsContainer;
        float _capsContainerSpeedX;
        float _capsContainerDisplacementAmmount;
        float _maxCapsContainerDisplacementAmmount = 20f;

        
        AnimateComponent _cap1;
        AnimateComponent _cap2;
        AnimateComponent _cap3;
        AnimateComponent _cap4;
        AnimateComponent _cap5;

        float _hiddenCapRotXDiff = 300f;
        Vector3 _hiddenCapScale = new Vector3();
        Vector3 _displayedCapScale = new Vector3(0, 0, 0);

        bool _displayingCaps = false;
		bool _displayingStage = false;
        float _timeBetweenCapDisplays = 0.05f;
        float _timeToNextCapDisplay = 0f;
        int _capDisplayIndex = 0;

        float _capsDisplayDelay = 0.8f;
        float _timeToDisplayCaps = 0f;

        List<AnimateComponent> _caps = new List<AnimateComponent>();
        List<Vector3> _capsAnimInitialRots = new List<Vector3>();
        List<Vector3> _capsAnimTargetRots = new List<Vector3>();

        float _capAnimationDuration = 0.8f;

        #endregion

        bool _touchReleased = true;
        Action _onTouchRelease;
        Vector2 _lastTouchPosition;

        bool _isVisible = false;
        public bool isVisible
        {
            get { return _isVisible; }
        }

        bool _isOpened = false;
        public bool isOpened
        {
            get { return _isOpened; }
        }

        public bool isEnabled
        {
            get { return gameObject.activeSelf; }
        }

        OnStageOpenDelegate _onStageOpen;
        OnStageCloseDelegate _onStageClose;

        public OnStageOpenDelegate OnStageOpen
        {
            get { return _onStageOpen; }
            set { _onStageOpen = value; }
        }

        public OnStageCloseDelegate OnStageClose
        {
            get { return _onStageClose; }
            set { _onStageClose = value; }
        }
                
       
        int _stageTimesDisplayed = 0;
        
        // Use this for initialization
        void Start()
        {
			ScreenManager.Instance.Initialize();

            _touchReleased = true;

            try
            {
               
				ScreenManager.Instance.Initialize();

                _capsContainer = this.Find("Caps", this.gameObject);

                _cap1 = this.FindAndResolveComponent<AnimateComponent>("Cap1", this.gameObject);
                _cap2 = this.FindAndResolveComponent<AnimateComponent>("Cap2", this.gameObject);
                _cap3 = this.FindAndResolveComponent<AnimateComponent>("Cap3", this.gameObject);
                _cap4 = this.FindAndResolveComponent<AnimateComponent>("Cap4", this.gameObject);
                _cap5 = this.FindAndResolveComponent<AnimateComponent>("Cap5", this.gameObject);

                if (_cap1 != null)
                    _caps.Add(_cap1);

                if (_cap2 != null)
                    _caps.Add(_cap2);

                if (_cap3 != null)
                    _caps.Add(_cap3);

                if (_cap4 != null)
                    _caps.Add(_cap4);

                if (_cap5 != null)
                    _caps.Add(_cap5);

                for (int c = 0; c < _caps.Count; c++)
                {

                    _capsAnimTargetRots.Add(_caps[c].transform.localRotation.eulerAngles);

                    Vector3 nRot = _caps[c].transform.localRotation.eulerAngles;
                    nRot.x += _hiddenCapRotXDiff;

                    _capsAnimInitialRots.Add(nRot);

                    if (_caps[c].transform.localScale.magnitude > _displayedCapScale.magnitude)
                        _displayedCapScale = _caps[c].transform.localScale;
                }

                _mainCamera = Camera.main;

                bool errorFound = false;
                
               

                HideCaps();

            }
            catch (Exception error)
            {
                Debug.LogError("An Error Ocurred: " + error.Message);
                Debug.LogError(error.StackTrace);
            }

        }

        // Update is called once per frame
        void Update()
        {
			bool tracking = FindObjectOfType<ARPlayerTrackableEventHandler>().IsTrackableVisible;

			if (tracking && !_displayingStage) {
				DisplayStage ();
			} else if(!tracking && _displayingStage){
				HideStage ();
			}

				// Used when you are on a full screen widget and want to prevent odd behaviour.
				if (FullScreenWidget.Instance.isRunning)
					return;
            
				if (_displayingCaps && _caps != null && _caps.Count > 0) {

					if (_timeToDisplayCaps > _capsDisplayDelay) {

						_timeToNextCapDisplay += Time.deltaTime;

						if (_timeToNextCapDisplay >= _timeBetweenCapDisplays && _capDisplayIndex < _caps.Count) {

							_timeToNextCapDisplay = 0;

							_caps [_capDisplayIndex].ScaleObject (_displayedCapScale, _capAnimationDuration, delegate {
								Debug.Log ("Cap Scaled");
							});

							_caps [_capDisplayIndex].RotateObject (_capsAnimTargetRots [_capDisplayIndex], _capAnimationDuration, delegate {
								Debug.Log ("Cap Rotated");
							});

							_capDisplayIndex++;

							if (_capDisplayIndex >= _caps.Count)
								_displayingCaps = false;
						}

					} else
						_timeToDisplayCaps += Time.deltaTime;

				}
            
				// Checks if we are not clicking any button component from the gui
				bool overGUIButton = false;

				if (EventSystem.current.currentSelectedGameObject != null) {
					Button pressedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button> ();

					if (pressedButton != null)
						overGUIButton = true;
				}


				#region TouchEvents | MouseEvents
				// Mouse, change it to touch event
				if (Input.GetMouseButton (0) && !CapCreditsWidget.Instance.isRunning && !overGUIButton) {


					float axisX = 0;

					if (_touchReleased) // Is the first pass when touched? then we dont set axisX to prevent weird rotation in the first seconds of touch.
                    axisX = 0;
					else
						axisX = -Input.GetAxis ("Mouse X") * 3;


					_capsContainerSpeedX = axisX;
					_capsContainer.transform.Rotate (0, _capsContainerSpeedX, 0, Space.World);

					_capsContainerDisplacementAmmount += Mathf.Abs (_capsContainerSpeedX);

					// For tick sound



			

					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

					RaycastHit hit = new RaycastHit ();

					//Debug.Log("Mouse 0 triggered");

					_onTouchRelease = null;

					bool pointerOverGameObject = false;

					if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
						if (EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId))
							pointerOverGameObject = true;
					} else {
						if (EventSystem.current.IsPointerOverGameObject ())
							pointerOverGameObject = true;
					}
                

					if (Physics.Raycast (ray, out hit) && !pointerOverGameObject) {

						//Debug.Log("collision!");

						if (hit.collider.gameObject == _cap1.gameObject && !_cap1.isTranslating) {
                        
						HandleCapAction (_cap1.gameObject, hit.point, GamesScreen.Instance);

						} else if (hit.collider.gameObject == _cap2.gameObject && !_cap2.isTranslating) {

						HandleCapAction (_cap2.gameObject, hit.point,GamesScreen.Instance);

						} else if (hit.collider.gameObject == _cap3.gameObject && !_cap3.isTranslating) {

						HandleCapAction (_cap3.gameObject, hit.point, GamesScreen.Instance);

						} else if (hit.collider.gameObject == _cap4.gameObject && !_cap4.isTranslating) {

							HandleCapAction (_cap4.gameObject, hit.point, VideoScreen.Instance);


						} else if (hit.collider.gameObject == _cap5.gameObject && !_cap5.isTranslating) {
						HandleCapAction (_cap5.gameObject, hit.point, GamesScreen.Instance);

						}

					}

					_touchReleased = false;

				} else {

					if (!_touchReleased && _onTouchRelease != null) {
						_onTouchRelease ();
					}
					_touchReleased = true;

					_capsContainerDisplacementAmmount = 0;

				}

				#endregion

				if (isVisible) {
					_capsContainer.transform.Rotate (0, _capsContainerSpeedX, 0, Space.World);
					_capsContainerSpeedX *= 0.7f;

               
				}

            
        }

        public void DisplayStage()
        {

            gameObject.SetActive(true);


            _displayingStage = true;


            DisplayCaps();

            _isVisible = true;


         
        }

        public void HideStage()
        {


            // Hack to force it to unactivate

            CloseStage();

            /*_animDisplayTime = 0;
            _animDisplayPercent = 0;*/

          

            HideCaps();

            _isVisible = false;
			_displayingStage = false;
            

        }

        private void DisplayCaps()
        {

            if (_caps == null || _caps.Count == 0)
                return;

            _displayingCaps = true;

        }

        private void HideCaps()
        {

            if (_caps == null || _caps.Count == 0)
                return;


            for (int c = 0; c < _caps.Count; c++)
            {

                Vector3 nRot = _capsAnimInitialRots[c];

                //_caps[c].transform.localRotation = Quaternion.Euler(nRot);

                //_caps[c].transform.localScale = _hiddenCapScale;

                _caps[c].RotateObject(nRot, _capAnimationDuration, null);

                _caps[c].ScaleObject(_hiddenCapScale, _capAnimationDuration, null);

                //_caps[c].StopAllAnimations();
            }

            _timeToNextCapDisplay = 0;
            _capDisplayIndex = 0;

            _timeToDisplayCaps = 0f;

            _displayingCaps = false;

        }

        public void OpenStage(Action onStageOpen = null)
        {


        }

        public void CloseStage(Action onStageClose = null)
        {

          


        }

        void HandleCapAction(GameObject cap, Vector3 touchHitPoint, IScreen screen)
        {

            Vector3 impactPoint = touchHitPoint;

            Vector3 capVector = cap.gameObject.transform.localPosition;

            cap.transform.position -= cap.transform.TransformVector(Vector3.forward * 0.5f);


            _onTouchRelease = delegate
            {

                cap.ResolveComponent<AnimateComponent>().TranslateObject(capVector, 0.4f, null);

                if (_capsContainerDisplacementAmmount < _maxCapsContainerDisplacementAmmount)
                {

					ScreenManager.Instance.stage = this;
					ScreenManager.Instance.DisplayScreen(screen);

                    

                }
                              

            };

        }

       

    }
}
