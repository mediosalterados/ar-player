﻿using UnityEngine;
using System.Collections;

using GLIB.Interface;
using GLIB.Audio;

using ARMediaCoca.Controllers.AR.WaterRings;

public class WaterRingMain : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // TODO Move following line to the main app when integrating
        SoundModule.Instance.Initialize();

        WaterRingsGame.Instance.Initialize();
              
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
