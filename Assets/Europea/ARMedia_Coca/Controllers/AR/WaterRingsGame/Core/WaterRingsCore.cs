﻿using UnityEngine;
using System.Collections;

using GLIB.Core;
using GLIB.Extended;

using System;
using System.Collections.Generic;

using GLIB.Audio;
using GLIB.Interface;

using ARMediaCoca.Views.AR.WaterRings.Screens;

public class WaterRingsCore : BackModule<WaterRingsCore> {

    GameObject _gameStage;

    Collider _bottleCollider;
    public Collider BottleCollider {
        get {
            return _bottleCollider;
        }
    }

    Collider _bottlePhysicalCollider;
    public Collider BottlePhysicalCollider {
        get {
            return _bottlePhysicalCollider;
        }
    }

    Collider _frontLimitCollider;
    public Collider FrontLimitCollider {
        get {
            return _frontLimitCollider;
        }
    }

    Collider _backLimitCollider;
    public Collider BackLimitCollider {
        get {
            return _backLimitCollider;
        }
    }

    Collider _leftLimitCollider;
    public Collider LeftLimitCollider {
        get {
            return _leftLimitCollider;
        }
    }

    Collider _rightLimitCollider;
    public Collider RightLimitCollider {
        get {
            return _rightLimitCollider;
        }
    }

    Collider _topLimitCollider;
    public Collider TopLimitCollider {
        get {
            return _topLimitCollider;
        }
    }

    Collider _bottomLimitCollider;
    public Collider BottomLimitCollider {
        get {
            return _bottomLimitCollider;
        }
    }

    int _gameScore = 0;
    public int gameScore {
        get {
            return _gameScore;
        }
    }

    const int INITIAL_GAMETIME = 40;
    float _gameTime = INITIAL_GAMETIME;
    public float gameTime {
        get {
            return _gameTime;
        }
    }

    Vector3 _leftPushDirection = new Vector3(-1, 1.5f, 0) * 300f;
    public Vector3 leftPushDirection {
        get { return _leftPushDirection; }
    }

    Vector3 _rightPushDirection = new Vector3(1, 1.5f, 0) * 300f;
    public Vector3 rightPushDirection {
        get { return _rightPushDirection; }
    }

    List<GameObject> _rings;

    bool _standBy = true;
    int _maxRings = 5;
    int _maxBadRings = 2;

    float _maxX = 8;
    float _minX = -8;
    float _maxY = 16;
    float _minY = 14;
    float _gapRadious = 2.5f;
    float _ringWidth = 3f;

    Vector3 _stagePosition = new Vector3();
    Vector3 _stageRotation = new Vector3();
    Vector3 _stageScale = new Vector3();
    Transform _stageParent = null;

    bool _isPaused = false;
    public bool isPaused {
        get { return _isPaused; }
    }

    // Game States
    public enum GameState : int {
        INSTRUCTIONS = 1,
        GAME = 2,
        RESULTS = 3
    }

    GameState _gameState;

    public GameState gameState {
        get { return _gameState; }
    }

    public void Initialize(Vector3 stagePosition, Vector3 stageRotation, Vector3 stageScale, Transform stageParent) {

        _stagePosition = stagePosition;
        _stageRotation = stageRotation;
        _stageScale = stageScale;
        _stageParent = stageParent;

        base.Initialize();

    }

    protected override void ProcessInitialization()
    {
        _isPaused = false;

        _maxBadRings = (int)Mathf.Floor((_maxRings * 0.4f));

        _gameStage = GameObject.Find("GameStage");

        if (_gameStage == null)
        {
            //throw new NullReferenceException("GameStage was not found, please add a GameObject within your project hierarchy");

            GameObject stagePrefab = Resources.Load<GameObject>(ResourcesPaths.OBJECT_WATERRINGS_GAMESTAGE);

            _gameStage = (GameObject)Instantiate(stagePrefab, new Vector3(), Quaternion.identity);

            if(_stageParent != null)
                _gameStage.transform.SetParent(_stageParent);

            _gameStage.transform.localPosition = _stagePosition;
            _gameStage.transform.localRotation = Quaternion.Euler(_stageRotation);
            _gameStage.transform.localScale = _stageScale;

        }

        _bottleCollider = this.FindAndResolveComponent<Collider>("Bottle", _gameStage);

        _frontLimitCollider = this.FindAndResolveComponent<Collider>("Front", _gameStage);
        _backLimitCollider = this.FindAndResolveComponent<Collider>("Back", _gameStage);
        _leftLimitCollider = this.FindAndResolveComponent<Collider>("Left", _gameStage);
        _rightLimitCollider = this.FindAndResolveComponent<Collider>("Right", _gameStage);
        _topLimitCollider = this.FindAndResolveComponent<Collider>("Top", _gameStage);
        _bottomLimitCollider = this.FindAndResolveComponent<Collider>("Bottom", _gameStage);
        _bottlePhysicalCollider = this.FindAndResolveComponent<Collider>("PhysicalCollider", _bottleCollider.gameObject);

        WaterRingsScreen.Instance.Initialize();
        //WaterRingsScreen.Instance.OpenInstructions();

        _gameState = GameState.INSTRUCTIONS;

        _standBy = true;
    }

    protected override void ProcessUpdate()
    {

        // Make GameStage always face the camera
        Vector3 mainCameraPos = Camera.main.transform.position;
        mainCameraPos.y = _gameStage.transform.position.y;

        _gameStage.transform.LookAt(mainCameraPos);

        if (_standBy) {

            /*if (_isPaused)
            {
                if (WaterRingsScreen.Instance.isResultsOpen)
                    WaterRingsScreen.Instance.CloseResults();
            }
            else {

                if (!WaterRingsScreen.Instance.isResultsOpen)
                    WaterRingsScreen.Instance.OpenResults();

            }*/

        }
        else
        {

            if (_isPaused)
            {
                /*if (WaterRingsScreen.Instance.isHUDOpen)
                    WaterRingsScreen.Instance.CloseHUD();*/
                               
            }
            else {

               /* if (!WaterRingsScreen.Instance.isHUDOpen)
                    WaterRingsScreen.Instance.OpenHUD();*/

                _gameTime -= Time.deltaTime;

                if (_gameTime <= 0)
                    EndGame();

            }


        }
    }

    protected override void ProcessTermination()
    {
        EndGame();

        GameObject.Destroy(_gameStage);

        _gameStage = null;

        WaterRingsScreen.Instance.Terminate();
        SoundModule.Instance.StopAllSounds();
    }

    public void AppendScore(int ammount) {

        if (ammount < 0)
        {
            SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_SCORE_DOWN), -1, SoundChannel.PlayPriority.HIGH, 0.3f);
            SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_HIT_NO), -1, SoundChannel.PlayPriority.HIGH, 0.7f);
        }
        else
        {
            SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_SCORE_UP), -1, SoundChannel.PlayPriority.HIGH, 0.5f);
            SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_HIT_YES_SHORT), -1, SoundChannel.PlayPriority.HIGH, 0.7f);
        }

            _gameScore += ammount;

        }

    public void AddTime(float time) {

        SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_TIME_BONUS), -1, SoundChannel.PlayPriority.HIGH, 0.5f);
        SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_HIT_YES_SHORT), -1, SoundChannel.PlayPriority.HIGH, 0.7f);
        _gameTime += time;

    }

    public void PushRings(Vector3 direction, bool transformToStageTransform = true) {

        if (!isRunning) {
            Debug.LogError("Error: WaterRingsCore module is not running, please initialize it first before using any of its methods.");
            return;
        }

        if(transformToStageTransform)
            direction = _gameStage.transform.TransformDirection(direction);

        WaterRingsGameRing[] rings = FindObjectsOfType<WaterRingsGameRing>();
        foreach (WaterRingsGameRing ring in rings)
        {
            Rigidbody body = ring.GetComponent<Rigidbody>();
            //direction.y = direction.y + (ring.transform.localPosition.y);
            body.AddForce(direction * (Mathf.Lerp(1f, 1.2f, UnityEngine.Random.value)) );
            body.AddTorque(UnityEngine.Random.rotation.eulerAngles * (Mathf.Lerp(0f, 1f, UnityEngine.Random.value)) );
            //body.AddExplosionForce(1000f, direction, 500f);
        }

        Debug.Log("Applied force");
    }
    
    public void StartGame() {

        if (!isRunning || !_standBy) {
            Debug.LogError("Core is not initialized, or the game has already started");
            return;
        }

        _gameScore = 0;
        _gameTime = INITIAL_GAMETIME;

        //WaterRingsScreen.Instance.OpenHUD();

        _rings = new List<GameObject>();
                
        List<Vector3> ringsPositions = BuildRandomRingPositions(_maxRings, _ringWidth, _minX, _maxX, _minY, _maxY);

        // Create the rings
        for (int r = 0; r < _maxRings; r++) {

            GameObject ringPrefab = Resources.Load<GameObject>(ResourcesPaths.PREFAB_RING);

            int timeRingChance = (int)Mathf.Floor((_maxRings * 0.2f));

            int badRingChance = (int)Mathf.Floor((_maxRings * 0.4f));

            float randomFloat = r <= (timeRingChance - 1) ? 0.09f : (r <= (badRingChance - 1) ? 0.30f : 0.5f); //UnityEngine.Random.value;

            //Debug.LogError(randomFloat);
            
            WaterRingsGameRing.RingType ringType = randomFloat <= 0.10f ? WaterRingsGameRing.RingType.TIME : (randomFloat <= 0.30f ? WaterRingsGameRing.RingType.BAD : WaterRingsGameRing.RingType.NORMAL);
            
            //Pick a random position
            Vector3 randPos = SliceRandomRingPosition(ringsPositions);

            CreateRing(ringPrefab, _gameStage, randPos, ringType);

        }

        SoundModule.Instance.PlayBGM(Resources.Load<AudioClip>(ResourcesPaths.BGM_1), 0, SoundChannel.PlayPriority.HIGH, 0.6f, 1, true);

        _standBy = false;

        _gameState = GameState.GAME;
    }

    public void EndGame() {

        if (!isRunning || _standBy) {
            Debug.LogError("Core is not initialized, or the game has already ended");
            return;
        }

        foreach (GameObject ring in _rings) {

            GameObject particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_RINGEXPLOSION);
            GameObject partInstance = Instantiate(particles);
            partInstance.transform.SetParent(_gameStage.transform);
            partInstance.transform.localPosition = new Vector3(ring.transform.localPosition.x, ring.transform.localPosition.y, -1.5f);

            Destroy(ring);
        }

        SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RING_DESTROY), -1, SoundChannel.PlayPriority.HIGH, 0.5f, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));
        SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RING_DESTROY), -1, SoundChannel.PlayPriority.HIGH, 0.5f, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));
        SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RING_DESTROY), -1, SoundChannel.PlayPriority.HIGH, 0.5f, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));

        _rings.Clear();

        //WaterRingsScreen.Instance.OpenResults();

        _gameState = GameState.RESULTS;

        _standBy = true;

        User.USER_POINTS += gameScore;

        //SoundModule.Instance.StopAllSounds();

    }

    public void CreateRing(GameObject ringPrefab, GameObject parentStage = null, Vector3 position = new Vector3(), WaterRingsGameRing.RingType type = WaterRingsGameRing.RingType.NORMAL) {

        GameObject ring = null;

        try
        {
            if (_rings == null)
                throw new NullReferenceException("Rings list not found, can't destroy a ring that is not registered in the rings list. Have you initialized this module?");

            if (ringPrefab == null)
                throw new NullReferenceException("Prefab Argument passed as null, couldn't instantiate a ring.");

            ring = (GameObject)Instantiate(ringPrefab, position, Quaternion.identity);

            WaterRingsGameRing ringBehaviour = ring.GetComponent<WaterRingsGameRing>();

            if(ringBehaviour == null)
                throw new NullReferenceException("No WaterRingsGameRing component found within the prefab, use a working prefab that includes the component.");

            ringBehaviour.ringType = type;

            Vector3 originalScale = ring.transform.localScale;

            if (parentStage != null)
                ring.transform.SetParent(_gameStage.transform);

            ring.transform.localScale = originalScale;

            ring.transform.localPosition = position;

            _rings.Add(ring);
        }
        catch (NullReferenceException e) {
            Debug.LogError(e.Message + "\n" + e.StackTrace);

            if (ring == null)
                Destroy(ring);
        }

    }

    public void DestroyRing(GameObject ring) {

        try
        {

            if (_rings == null)
                throw new NullReferenceException("Rings list not found, can't destroy a ring that is not registered in the rings list. Have you initialized this module?");

            int ringIndex = -1;

            for (int r = 0; r < _rings.Count; r++)
            {

                if (_rings[r] == ring)
                {
                    ringIndex = r;
                    break;
                }

            }

            if (ringIndex >= 0)
            {

                _rings.RemoveAt(ringIndex);

                GameObject ringDestroyPart = Resources.Load<GameObject>("");
                         
                GameObject particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_RINGEXPLOSION);
                GameObject partInstance = Instantiate(particles);
                partInstance.transform.SetParent(_gameStage.transform);
                partInstance.transform.localPosition = new Vector3(ring.transform.localPosition.x, ring.transform.localPosition.y, 1.5f);
                
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RING_DESTROY), -1, SoundChannel.PlayPriority.HIGH, 0.5f, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));

                string cotText = "";
                Color cotColor = Color.white;

                WaterRingsGameRing waterRing = ring.GetComponent<WaterRingsGameRing>();

                if (waterRing) {
                    cotText = waterRing.ringScoreTimeValue.ToString();
                    cotColor = waterRing.ringType == WaterRingsGameRing.RingType.BAD ? new Color(1, 0.2f, 0.2f, 1) : (waterRing.ringType == WaterRingsGameRing.RingType.NORMAL ? Color.white : new Color(0.39f, 1, 0.31f));
                }

                CallOutText cot = new CallOutText(cotText, Resources.Load<Font>("Fonts/Gotham-BoldIta"), 80, cotColor, 1f, CallOutText.AnimationStyle.TRANSLATE_AND_SCALE, 120, WaterRingsScreen.Instance.DisplayObject.GetComponent<RectTransform>(), ring.transform.localPosition);

                Destroy(ring);

                // Check max number of bad rings, dont be so cruel
                int badRings = 0;

                foreach (GameObject cRing in _rings) {

                    WaterRingsGameRing ringBehaviour = cRing.GetComponent<WaterRingsGameRing>();

                    if (ringBehaviour != null && ringBehaviour.ringType == WaterRingsGameRing.RingType.BAD)
                        badRings++;

                }
                //

                float randomFloat = Mathf.Clamp(UnityEngine.Random.value, badRings >= _maxBadRings ? 0.21f : 0, 1); 

                WaterRingsGameRing.RingType ringType = randomFloat <= 0.20 ? WaterRingsGameRing.RingType.BAD : (randomFloat <= 0.30 ? WaterRingsGameRing.RingType.TIME : WaterRingsGameRing.RingType.NORMAL);

                CreateRing(Resources.Load<GameObject>(ResourcesPaths.PREFAB_RING), 
                    _gameStage, 
                    SliceRandomRingPosition(BuildRandomRingPositions(_maxRings, _ringWidth, _minX, _maxX, _minY, _maxY)), 
                    ringType 
                );

            }
        }
        catch (NullReferenceException e) {
            Debug.LogError(e.Message + "\n" + e.StackTrace);
        }

    }

    public List<Vector3> BuildRandomRingPositions(int maxRings, float ringWidth, float minX, float maxX, float minY, float maxY) {

        List<Vector3> ringsPositions = new List<Vector3>();

        int maxRingsPositions = maxRings * 2;

        int maxRingsPerRow = (int)Mathf.Floor((maxX - minX) / ringWidth);

        Debug.Log("Rings Per Row: " + maxRingsPerRow);

        int maxRingsRows = Mathf.CeilToInt(((maxRingsPositions + 0f) / (maxRingsPerRow + 0f)));

        Debug.Log("Max Rings Rows: " + maxRingsRows);

        for (int rR = 0; rR < maxRingsRows; rR++)
        {

            for (int rC = 0; rC < maxRingsPerRow; rC++)
            {

                float ringPosX = minX;

                Vector3 nPos = new Vector3(minX + (ringWidth * rC), maxY - (ringWidth * rR));

                ringsPositions.Add(nPos);

                if (ringsPositions.Count >= (maxRingsPositions))
                    break;
            }

        }

        return ringsPositions;

    }

    public Vector3 SliceRandomRingPosition(List<Vector3> randomPositions) {

        Vector3 randomPos = new Vector3();

        if (randomPositions == null) {
            Debug.LogError("randomPosition argument is null, aborting.");
            return new Vector3();
        }

        int randIndex = Mathf.CeilToInt(Mathf.Lerp(0f, (randomPositions.Count - 1f), UnityEngine.Random.value));

        if (randIndex < randomPositions.Count) {
            randomPos = randomPositions[randIndex];
            randomPositions.RemoveAt(randIndex);
        }

        return randomPos;

    }

    public void PauseGame() {
        //Time.timeScale = 0;
        // Make all rings to stop moving
        foreach (GameObject ring in _rings) {

            Rigidbody rigidBody = ring.GetComponent<Rigidbody>();

            if (rigidBody)
                rigidBody.isKinematic = true;

        }

        SoundModule.Instance.PauseBGMChannel(0);

        _isPaused = true;

    }

    public void UnPauseGame() {
        //Time.timeScale = 1;
        // Make all rings to start moving again
        foreach (GameObject ring in _rings)
        {

            Rigidbody rigidBody = ring.GetComponent<Rigidbody>();

            if (rigidBody)
                rigidBody.isKinematic = false;

        }

        SoundModule.Instance.UnPauseBGMChannel(0);

        _isPaused = false;


    }

}
