﻿using UnityEngine;
using System.Collections;

using GLIB.Core;
using System;

using ARMediaCoca.Views.Main.Screens;
using ARMediaCoca.Models.Interfaces;


namespace ARMediaCoca.Controllers.AR.WaterRings { 


    public class WaterRingsGame : BackModule<WaterRingsGame>, IGame
    {

        bool _stageParameters = false;

        Vector3 _stagePosition;
        Vector3 _stageRotation;
        Vector3 _stageScale;

        Transform _stageParent;

        IStage _cokeMenuStage;

        bool _isPaused = false;
        public bool isPaused
        {
            get { return _isPaused; }
        }

        public void Initialize(Vector3 stagePosition, Vector3 stageRotation, Vector3 stageScale, Transform stageParent)
        {

            _stageParameters = true;

            _stagePosition = stagePosition;
            _stageRotation = stageRotation;
            _stageScale = stageScale;
            _stageParent = stageParent;

            base.Initialize();
        }

        protected override void ProcessInitialization()
        {
            if (_stageParameters)
            {
                WaterRingsCore.Instance.Initialize(_stagePosition, _stageRotation, _stageScale, _stageParent);
            }
            else
                WaterRingsCore.Instance.Initialize();

            _cokeMenuStage = InterfaceHelper.FindObject<IStage>();

            if (_cokeMenuStage != null)
                _cokeMenuStage.HideStage();

            _isPaused = false;
        }

        protected override void ProcessUpdate()
        {

        }

        protected override void ProcessTermination()
        {
            WaterRingsCore.Instance.Terminate();

            if (_cokeMenuStage != null)
                _cokeMenuStage.DisplayStage();
        }

        public void PauseGame()
        {
            WaterRingsCore.Instance.PauseGame();
            _isPaused = true;
        }

        public void UnPauseGame()
        {
            WaterRingsCore.Instance.UnPauseGame();
            _isPaused = false;
        }
        
    }
}
