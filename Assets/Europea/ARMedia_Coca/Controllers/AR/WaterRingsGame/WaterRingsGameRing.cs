﻿using UnityEngine;
using System.Collections;

using GLIB.Audio;

public class WaterRingsGameRing : MonoBehaviour {

    public enum RingType : int {
        NORMAL = -1,
        BAD = 0,
        TIME = 1
    }

    RingType _ringType = RingType.NORMAL;
    public RingType ringType {
        get {
            return _ringType;
        }
        set {
            _ringType = value;
        }
    }

    public int ringScoreTimeValue {
        get {
            return ringType == RingType.BAD ? -10 : (ringType == RingType.NORMAL ? 10 : 10);
        }
    }

    bool _hitFXPlayed = false;
    Collider _hitFXCollider;

    Renderer _ringRenderer;

    float _introAnimTime;
    float _introAnimDuration = 1f;

	// Use this for initialization
	void Start () {

        Material ringMaterial;

        GameObject particles = null;

        if (_ringType == RingType.TIME)
        {
            ringMaterial = Resources.Load<Material>(ResourcesPaths.MAT_TIMERING);

            particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_TIMERINGTRAIL);
        }
        else if (_ringType == RingType.BAD)
        {
            ringMaterial = Resources.Load<Material>(ResourcesPaths.MAT_BADRING);

            particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_BADRINGTRAIL);
        }
        else {

            ringMaterial = Resources.Load<Material>(ResourcesPaths.MAT_NORMALRING);

            //particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_NORMALRINGTRAIL);
        }

        _ringRenderer = this.gameObject.GetComponent<Renderer>();

        if (_ringRenderer != null && ringMaterial != null) {
            _ringRenderer.material = ringMaterial;
            _ringRenderer.material.SetFloat("_Cutoff", 1f);
        }

        if (particles != null)
        {
            GameObject partInstance = Instantiate(particles);

            partInstance.transform.SetParent(this.transform);

            partInstance.transform.localPosition = new Vector3();
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (_ringRenderer != null && _introAnimTime < _introAnimDuration) {

            _introAnimTime += Time.deltaTime;

            float percent = _introAnimTime / _introAnimDuration;

            _ringRenderer.material.SetFloat("_Cutoff", Mathf.Lerp(1f, 0f, percent));
        
            if(percent >= 1)
                _ringRenderer.material.SetFloat("_Cutoff", 0f);
        }
        
	}

    void OnTriggerEnter(Collider colliderObj)
    {
       

        if(WaterRingsCore.Instance.isRunning && colliderObj == WaterRingsCore.Instance.BottleCollider) {

            if (_ringType == RingType.NORMAL)
                WaterRingsCore.Instance.AppendScore(ringScoreTimeValue);
            else if (_ringType == RingType.TIME)
                WaterRingsCore.Instance.AddTime(ringScoreTimeValue);
            else if (_ringType == RingType.BAD)
                WaterRingsCore.Instance.AppendScore(ringScoreTimeValue);

            WaterRingsCore.Instance.DestroyRing(this.gameObject);

        }
               

    }

    void OnCollisionEnter(Collision objCollision) {

        if (objCollision.collider == WaterRingsCore.Instance.LeftLimitCollider ||
            objCollision.collider == WaterRingsCore.Instance.RightLimitCollider ||
            objCollision.collider == WaterRingsCore.Instance.TopLimitCollider ||
            objCollision.collider == WaterRingsCore.Instance.BottomLimitCollider || 
            objCollision.collider == WaterRingsCore.Instance.BottlePhysicalCollider)
        {

            if (_hitFXPlayed || objCollision.relativeVelocity.magnitude < 10f)
                return;

            GameObject particles = Resources.Load<GameObject>(ResourcesPaths.PREFAB_PARTICLES_RINGHIT);

            /*GameObject partInstance = Instantiate(particles);
            partInstance.transform.SetParent(this.gameObject.transform.parent);

            partInstance.transform.position = objCollision.contacts[0].point;*/

            _hitFXCollider = objCollision.collider;

            string soundPath = objCollision.collider == WaterRingsCore.Instance.BottlePhysicalCollider ? ResourcesPaths.SFX_BOTTLE_HIT : ResourcesPaths.SFX_GLASS_HIT;

            AudioClip soundClip = Resources.Load<AudioClip>(soundPath);

            //WaterRingsCore.Instance.PlaySFX(soundPath, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));
            SoundModule.Instance.PlaySFX(soundClip, -1, SoundChannel.PlayPriority.NORMAL, 0.1f, Mathf.Lerp(0.9f, 1.1f, UnityEngine.Random.value));

            _hitFXPlayed = true;
            
        }
    }

    void OnCollisionExit(Collision objCollision) {

        if (_hitFXCollider != null && objCollision.collider == _hitFXCollider) {
            _hitFXCollider = null;
            _hitFXPlayed = false;
        }

    }

    void OnTriggerExit(Collider colliderObj)
    {


        if (_hitFXPlayed)
            _hitFXPlayed = false;

    }

    void OnDestroy() {
                
    }


}
