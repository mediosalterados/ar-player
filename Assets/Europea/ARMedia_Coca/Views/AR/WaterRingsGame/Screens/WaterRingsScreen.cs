﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;
using GLIB.Audio;
using System;

using ARMediaCoca.Controllers.AR.WaterRings;
using ARMediaCoca.Views.AR.WaterRings.Widgets;

namespace ARMediaCoca.Views.AR.WaterRings.Screens
{
    
    public class WaterRingsScreen : UIModule<WaterRingsScreen>
    {

        Text _scoreTextComponent;
        bool _exitScreenPrompt;

        public bool isHUDOpen
        {
            get { return WaterRingsHUD.Instance.isRunning; }
        }

        public bool isInstructionsOpen
        {
            get { return WaterRingsInstructions.Instance.isRunning; }
        }

        public bool isResultsOpen {
            get { return WaterRingsResults.Instance.isRunning; }
        }

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_AR_WRG_SCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition();
            }
        }

        protected override void ProcessInitialization()
        {
            NotificationSystem.Instance.TemplatePath = ResourcesPaths.TEMPLATE_GRIMOIRE_NOTIFICATIONSYSTEM;

            Button _exitBtn = this.FindAndResolveComponent<Button>("Menu<Button>", DisplayObject);

            _exitBtn.onClick.AddListener(delegate {
                ExitGame();
            });

            _exitScreenPrompt = false;
        }

        protected override void ProcessUpdate()
        {

            if (WaterRingsCore.Instance.isPaused) {

                if (WaterRingsCore.Instance.gameState == WaterRingsCore.GameState.INSTRUCTIONS)
                {
                    if (WaterRingsInstructions.Instance.isRunning)
                        WaterRingsInstructions.Instance.Terminate();
                }
                else if (WaterRingsCore.Instance.gameState == WaterRingsCore.GameState.RESULTS)
                {
                    if (WaterRingsResults.Instance.isRunning)
                        WaterRingsResults.Instance.Terminate();
                }

                if (WaterRingsHUD.Instance.isRunning)
                    WaterRingsHUD.Instance.Terminate();

            }
            else {

                if (WaterRingsCore.Instance.gameState == WaterRingsCore.GameState.INSTRUCTIONS)
                {
                    if (!WaterRingsInstructions.Instance.isRunning)
                        WaterRingsInstructions.Instance.Initialize();
                }
                else if (WaterRingsCore.Instance.gameState == WaterRingsCore.GameState.RESULTS)
                {
                    if (!WaterRingsResults.Instance.isRunning)
                        WaterRingsResults.Instance.Initialize();
                }

                if (!WaterRingsHUD.Instance.isRunning)
                    WaterRingsHUD.Instance.Initialize();

            }

            if (Input.GetKeyDown(KeyCode.Escape)) {
                if (!_exitScreenPrompt)
                    ExitGame();
                else {
                    WaterRingsGame.Instance.Terminate();
                    NotificationSystem.Instance.Terminate();
                }
            }

        }

        protected override void ProcessTermination()
        {
            WaterRingsHUD.Instance.Terminate();
            WaterRingsInstructions.Instance.Terminate();
            WaterRingsResults.Instance.Terminate();
        }

        void ExitGame() {

            Action onAccept = delegate {
                Debug.Log("Aceptar");
                WaterRingsGame.Instance.Terminate();
                NotificationSystem.Instance.Terminate();
                _exitScreenPrompt = false;
            };

            Action onDecline = delegate {
                Debug.Log("Decline");
                NotificationSystem.Instance.Terminate();
                _exitScreenPrompt = false;
            };

            NotificationSystem.Instance.PromptAction("¿Salir del juego?\n", delegate {
                onAccept();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            }, delegate {
                onDecline();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });
            SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);

            _exitScreenPrompt = true;

        }

        // Widgets
        /*public void OpenHUD()
        {
            WaterRingsHUD.Instance.Initialize();
        }

        public void CloseHUD()
        {
            WaterRingsHUD.Instance.Terminate();
        }

        public void OpenInstructions()
        {
            WaterRingsInstructions.Instance.Initialize();
        }

        public void CloseInstructions() {
            WaterRingsInstructions.Instance.Terminate();
        }

        public void OpenResults()
        {
            WaterRingsResults.Instance.Initialize();
        }

        public void CloseResults() {
            WaterRingsResults.Instance.Terminate();
        }*/

    }


}