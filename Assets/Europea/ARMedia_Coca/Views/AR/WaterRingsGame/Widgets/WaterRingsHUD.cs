﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Audio;
using GLIB.Utils;

using System;

using ARMediaCoca.Controllers.AR.WaterRings;
using ARMediaCoca.Models.Interfaces;

namespace ARMediaCoca.Views.AR.WaterRings.Widgets {

    public class WaterRingsHUD : UIModule<WaterRingsHUD> {

        Text _scoreTextComponent;
        Text _timeTextComponent;

        Button _leftPushButton;
        Animator _leftPushFX;

        Button _rightPushButton;
        Animator _rightPushFX;

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_AR_WRG_WIDGET_HUD;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization() {

            _scoreTextComponent = this.FindAndResolveComponent<Text>("Score<Text>", DisplayObject);
            _timeTextComponent = this.FindAndResolveComponent<Text>("Time<Text>", DisplayObject);

            _leftPushButton = this.FindAndResolveComponent<Button>("LeftPush<Button>", DisplayObject);
            _leftPushFX = this.FindAndResolveComponent<Animator>("BubblesFX", _leftPushButton.gameObject);

            _leftPushButton.onClick.AddListener(delegate {
                _leftPushFX.Play("BubblesFX");
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_WATER_PUSH), -1, SoundChannel.PlayPriority.NORMAL, 1, Mathf.Lerp(0.9f, 1f, UnityEngine.Random.value));
                WaterRingsCore.Instance.PushRings(WaterRingsCore.Instance.leftPushDirection);
            });

            _rightPushButton = this.FindAndResolveComponent<Button>("RightPush<Button>", DisplayObject);
            _rightPushFX = this.FindAndResolveComponent<Animator>("BubblesFX", _rightPushButton.gameObject);

            _rightPushButton.onClick.AddListener(delegate {
                _rightPushFX.Play("BubblesFX");
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_WATER_PUSH), -1, SoundChannel.PlayPriority.NORMAL, 1, Mathf.Lerp(0.9f, 1f, UnityEngine.Random.value));
                WaterRingsCore.Instance.PushRings(WaterRingsCore.Instance.rightPushDirection);
            });

            
        }

        protected override void ProcessUpdate()
        {
            _scoreTextComponent.text = "Score\n" + WaterRingsCore.Instance.gameScore;

            float gameTime = WaterRingsCore.Instance.gameTime;

            int timeMinutes = (int)(gameTime / 60);
            int timeSeconds = (int)(gameTime %= 60);
            
            string formattedTime = (timeMinutes < 10 ? "0" : "") + timeMinutes.ToString() + ":" + (timeSeconds < 10 ? "0" : "") + timeSeconds.ToString();

            _timeTextComponent.text = "Time\n" + formattedTime;
        }

        protected override void ProcessTermination()
        {

        }

    }

}