﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;

using ARMediaCoca.Controllers.AR.WaterRings;
using ARMediaCoca.Views.AR.WaterRings.Screens;
using GLIB.Audio;

namespace ARMediaCoca.Views.AR.WaterRings.Widgets
{

    public class WaterRingsResults : UIModule<WaterRingsResults>
    {

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_AR_WRG_WIDGET_RESULTS;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                if (WaterRingsScreen.Instance.isRunning)
                    return WaterRingsScreen.Instance.DisplayObject.transform;
                else
                    return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            Button replayBtn = this.FindAndResolveComponent<Button>("Replay<Button>", DisplayObject);
            replayBtn.onClick.AddListener(delegate {
                WaterRingsCore.Instance.StartGame();
                Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button cancelBtn = this.FindAndResolveComponent<Button>("Cancel<Button>", DisplayObject);
            cancelBtn.onClick.AddListener(delegate {
                WaterRingsGame.Instance.Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            int intGameScore = WaterRingsCore.Instance.gameScore;

            Text gameScore = this.FindAndResolveComponent<Text>("GameScore<Text>", DisplayObject);
            gameScore.text = intGameScore.ToString();

            if (intGameScore <= 0)
            {
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RESULTS_BAD), -1, SoundChannel.PlayPriority.HIGH, 0.6f);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_HIT_NO), -1, SoundChannel.PlayPriority.HIGH, 0.7f);
            }
            else
            {
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RESULTS_GOOD), -1, SoundChannel.PlayPriority.HIGH, 0.6f);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_RESULTS_LAUGH), -1, SoundChannel.PlayPriority.HIGH, 0.9f);
            }

        }

        protected override void ProcessUpdate()
        {

        }

        protected override void ProcessTermination()
        {

        }
    }

}