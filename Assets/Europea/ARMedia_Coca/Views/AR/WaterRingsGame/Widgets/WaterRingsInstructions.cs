﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

using ARMediaCoca.Views.AR.WaterRings.Screens;
using GLIB.Audio;

namespace ARMediaCoca.Views.AR.WaterRings.Widgets
{

    public class WaterRingsInstructions : UIModule<WaterRingsInstructions>
    {

        GameObject _loadingDisplay;


        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_AR_WRG_WIDGET_INSTRUCTIONS;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                if (WaterRingsScreen.Instance.isRunning)
                    return WaterRingsScreen.Instance.DisplayObject.transform;
                else
                    return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
           

            _loadingDisplay = this.Find("Loading<Image>", DisplayObject);

            Button playBtn = this.FindAndResolveComponent<Button>("Play<Button>", DisplayObject);
            playBtn.onClick.AddListener(delegate {
                _loadingDisplay.transform.localScale = new Vector3(1, 1, 1);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
                StartCoroutine(LaunchGame());
            });

        }

        protected override void ProcessUpdate()
        {
           
        }

        protected override void ProcessTermination()
        {
            
        }

        IEnumerator LaunchGame() {

            yield return new WaitForSeconds(0.3f);

            WaterRingsCore.Instance.StartGame();

            Terminate();
        }

    }
}
