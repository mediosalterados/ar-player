﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;

using System;

using ARMediaCoca.Models.Interfaces;
using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Controllers.EventHandlers;
using ARMediaCoca.Controllers.AR.WaterRings;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens
{

    public class ControlsScreen : UIModule<ControlsScreen>
    {


        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_CONTROLSSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {

                return null;

            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
            closeButton.onClick.AddListener(delegate {
                Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Debug.Log("Controls Screen Initialized");
        }

        protected override void ProcessUpdate()
        {

        }

        protected override void ProcessTermination()
        {
            Debug.Log("Controls Screen Terminated");
        }

    }
}
