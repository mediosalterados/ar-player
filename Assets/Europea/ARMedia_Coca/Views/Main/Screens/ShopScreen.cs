﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Utils;
using GLIB.Extended;

using System;

using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Models.Interfaces;

using ARMediaCoca.Views.Main.Widgets;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens
{

    public class ShopScreen : UIModule<ShopScreen>, IScreen
    {

        GameObject _productsContainer;

        public int pointsToRedeem {
            get { return _pointsToRedeem; }
        }
        int _pointsToRedeem;

        Image _productDisplayer;
        Text _productCost;
        Text _userPoints;

        Dictionary<GameObject, CokeProduct> _products;

        struct CokeProduct {

            public int price;
            public Button triggerButton;
            public Image imageComponent;
            public Image hightlightImageComponent;
            public Sprite normalSprite;
            public Sprite selectedSprite;
            public Sprite largeSprite;

        }

        enum CokeProductsPrices : int {

            SODA = 80,
            BALLON = 150,
            PHONE_COVER = 90,
            TICKET = 450,
            RADIO = 350,
            USB = 170

        }

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_SHOPSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {

                GameObject parent = GameObject.Find("3DCanvas");

                if (parent)
                    return parent.transform;
                else
                    return null;

            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            DisplayObject.transform.localRotation = Quaternion.identity;
            DisplayObject.transform.localPosition = new Vector3();

            _productsContainer = this.Find("Products", DisplayObject);

            _products = new Dictionary<GameObject, CokeProduct>();

            _productDisplayer = this.FindAndResolveComponent<Image>("SelectedProduct<Image>", DisplayObject);

            _productCost = this.FindAndResolveComponent<Text>("ProductCost<Text>", DisplayObject);

            NotificationSystem.OnMessageAcceptDelegate onRedeemAccept = delegate {

                if (RedeemPoints(_pointsToRedeem))
                {
                    SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
                    NotificationSystem.Instance.Terminate();
                }
                else
                    NotificationSystem.Instance.NotifyMessage(ResourcesPaths.POINTS_REDEEM_FAILURE, null, delegate { SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); });
            };

            NotificationSystem.OnMessageDeclineDelegate onRedeemDecline = delegate
            {
                NotificationSystem.Instance.Terminate();
            };
            
            Button redeemBtn = this.FindAndResolveComponent<Button>("Redeem<Button>", DisplayObject);
            redeemBtn.onClick.AddListener(delegate {
                if(User.USER_POINTS >= _pointsToRedeem)
                    NotificationSystem.Instance.PromptAction(ResourcesPaths.POINTS_REDEEM_PROMPTA + _pointsToRedeem + ResourcesPaths.POINTS_REDEEM_PROMPTB, onRedeemAccept, onRedeemDecline);
                else
                    NotificationSystem.Instance.NotifyMessage(ResourcesPaths.POINTS_REDEEM_FAILURE, null, delegate { SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); });
            });

            //Button[] productButtons = _productsContainer.GetComponentsInChildren<Button>();

            GameObject product1 = this.Find("Product1", _productsContainer);
            CokeProduct coke1 = new CokeProduct();
            coke1.price = (int)CokeProductsPrices.SODA;
            coke1.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product1);
            coke1.imageComponent = coke1.triggerButton.image;
            coke1.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product1);
            coke1.normalSprite = coke1.triggerButton.image.sprite;
            coke1.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_SODA_H);
            coke1.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_SODA_LARGE);
            coke1.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke1);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            GameObject product2 = this.Find("Product2", _productsContainer);
            CokeProduct coke2 = new CokeProduct();
            coke2.price = (int)CokeProductsPrices.BALLON;
            coke2.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product2);
            coke2.imageComponent = coke2.triggerButton.image;
            coke2.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product2);
            coke2.normalSprite = coke2.triggerButton.image.sprite;
            coke2.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_BALLON_H);
            coke2.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_BALLON_LARGE);
            coke2.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke2);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            GameObject product3 = this.Find("Product3", _productsContainer);
            CokeProduct coke3 = new CokeProduct();
            coke3.price = (int)CokeProductsPrices.PHONE_COVER;
            coke3.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product3);
            coke3.imageComponent = coke3.triggerButton.image;
            coke3.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product3);
            coke3.normalSprite = coke3.triggerButton.image.sprite;
            coke3.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_COVER_H);
            coke3.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_COVER_LARGE);
            coke3.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke3);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            GameObject product4 = this.Find("Product4", _productsContainer);
            CokeProduct coke4 = new CokeProduct();
            coke4.price = (int)CokeProductsPrices.USB;
            coke4.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product4);
            coke4.imageComponent = coke4.triggerButton.image;
            coke4.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product4);
            coke4.normalSprite = coke4.triggerButton.image.sprite;
            coke4.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_USB_H);
            coke4.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_USB_LARGE);
            coke4.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke4);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            GameObject product5 = this.Find("Product5", _productsContainer);
            CokeProduct coke5 = new CokeProduct();
            coke5.price = (int)CokeProductsPrices.RADIO;
            coke5.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product5);
            coke5.imageComponent = coke5.triggerButton.image;
            coke5.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product5);
            coke5.normalSprite = coke5.triggerButton.image.sprite;
            coke5.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_RADIO_H);
            coke5.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_RADIO_LARGE);
            coke5.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke5);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            GameObject product6 = this.Find("Product6", _productsContainer);
            CokeProduct coke6 = new CokeProduct();
            coke6.price = (int)CokeProductsPrices.TICKET;
            coke6.triggerButton = this.FindAndResolveComponent<Button>("Trigger<Button>", product6);
            coke6.imageComponent = coke6.triggerButton.image;
            coke6.hightlightImageComponent = this.FindAndResolveComponent<Image>("Highlight<Image>", product6);
            coke6.normalSprite = coke6.triggerButton.image.sprite;
            coke6.selectedSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_TICKET_H);
            coke6.largeSprite = AtlasManager.getSprite(ResourcesPaths.SPRITE_NAME_TICKET_LARGE);
            coke6.triggerButton.onClick.AddListener(delegate {
                SelectProduct(coke6);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            _products.Add(product1, coke1);
            _products.Add(product2, coke2);
            _products.Add(product3, coke3);
            _products.Add(product4, coke4);
            _products.Add(product5, coke5);
            _products.Add(product6, coke6);
            
            HandleProductHighlights(_products, User.USER_POINTS);

            SelectProduct(coke1);

            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
            closeButton.onClick.AddListener(delegate {
                ScreenManager.Instance.CloseScreen();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            _userPoints = this.FindAndResolveComponent<Text>("UserPoints<Text>", DisplayObject);           

            Debug.Log("Shop Screen Initialized");

        }

        protected override void ProcessUpdate()
        {
            _userPoints.text = User.USER_POINTS.ToString();
        }

        protected override void ProcessTermination()
        {
            Debug.Log("Shop Screen Terminated");
        }

        public bool isScreenRunning
        {
            get
            {
                return isRunning;
            }
        }

        public void StartScreen()
        {
            Initialize();
        }

        public void EndScreen(bool force = false)
        {
            Terminate(force);
        }

        void SelectProduct(CokeProduct product) {

            ResetPrizeButtonStatus(_products);

            product.imageComponent.sprite = product.selectedSprite;

            _pointsToRedeem = product.price;

            _productCost.text = product.price.ToString();

            _productDisplayer.sprite = product.largeSprite;

            Animator productDisplayerAnimator = _productDisplayer.GetComponent<Animator>();                       

            productDisplayerAnimator.Play("SelectProduct", -1, 0f);

        }

        void ResetPrizeButtonStatus(Dictionary<GameObject, CokeProduct> dictionary) {

            foreach (KeyValuePair<GameObject, CokeProduct> entry in dictionary)
                entry.Value.imageComponent.sprite = entry.Value.normalSprite;

        }

        public bool RedeemPoints(int points) {

            if (User.USER_POINTS >= points)
            {
                User.USER_POINTS -= points;
                HandleProductHighlights(_products, User.USER_POINTS);

                //NotificationSystem.Instance.NotifyMessage(ResourcesPaths.POINTS_REDEEM_SUCESS, null, delegate { SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); } );

                ProductRedeemWidget.Instance.Initialize();

                return true;
            }
            else
                return false;               
            
        }

        void HandleProductHighlights(Dictionary<GameObject, CokeProduct> dictionary, float userPoints ) {

            foreach (KeyValuePair<GameObject, CokeProduct> entry in dictionary)
                entry.Value.hightlightImageComponent.enabled = userPoints >= entry.Value.price ? true : false;

        }

    }
}
