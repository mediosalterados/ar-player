﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;

using System;

using ARMediaCoca.Models.Interfaces;
using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Views.Main.Widgets;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens
{

    public class AccountScreen : UIModule<AccountScreen>, IScreen
    {

        Text _userPoints;

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_ACCOUNTSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {

                GameObject parent = GameObject.Find("3DCanvas");

                if (parent)
                    return parent.transform;
                else
                    return null;

            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            DisplayObject.transform.localRotation = Quaternion.identity;
            DisplayObject.transform.localPosition = new Vector3();

            Button shopButton = this.FindAndResolveComponent<Button>("Shop<Button>", DisplayObject);
            shopButton.onClick.AddListener(delegate {
                ScreenManager.Instance.DisplayScreen(ShopScreen.Instance);
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button capCreditButton = this.FindAndResolveComponent<Button>("AddCredits<Button>", DisplayObject);
            capCreditButton.onClick.AddListener(delegate {
                CapCreditsWidget.Instance.Initialize();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            _userPoints = this.FindAndResolveComponent<Text>("UserPoints<Text>", DisplayObject);
            _userPoints.text = User.USER_POINTS.ToString();

            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
            closeButton.onClick.AddListener(delegate {
                ScreenManager.Instance.CloseScreen();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Debug.Log("Account Screen Initialized");
        }

        protected override void ProcessUpdate()
        {
            //_userCredits.text = User.USER_CREDITS.ToString();
            _userPoints.text = User.USER_POINTS.ToString();
        }

        protected override void ProcessTermination()
        {
            Debug.Log("Account Screen Terminated");
        }

        public bool isScreenRunning {
            get {
                return isRunning;
            }
        }

        public void StartScreen() {
            Initialize();
        }

        public void EndScreen(bool force = false) {
            Terminate(force);
        }
    }
}
