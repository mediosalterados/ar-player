﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

using ARMediaCoca.Controllers.EventHandlers;
using ARMediaCoca.Models.Interfaces;

using GLIB.Interface;
using GLIB.Extended;

using Vuforia;

using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Views.Main.Widgets;
using ARPlayer.Controllers.Main;

using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens
{

    public class ARScreen : UIModule<ARScreen>
    {

        IStage _stage;
		ARPlayerTrackableEventHandler _cylinderTrackeable;

        bool _vuforiaStarted;
        

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_ARSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(UIModule<ARScreen>.Transition.InOutAnimations.SCALE);
            }
        }

       
        protected override void ProcessInitialization()
        {
            Debug.Log("ARScreen Initialized");

            try
            {
                FindObjectOfType<VuforiaBehaviour>().enabled = true;
               // FindObjectOfType<VideoBackgroundManager>().enabled = true;
                ScreenManager.Instance.Initialize();

                VuforiaAbstractBehaviour vuforia = FindObjectOfType<VuforiaAbstractBehaviour>();
               //vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
               // vuforia.RegisterOnPauseCallback(OnVuforiaPaused);
                
            }
            catch (Exception error) {
                Debug.LogError("Error, VuforiaBehavior not found : "+ error.Message);
                Debug.LogError(error.StackTrace);
            }

            // Get a IStage Object in the scene.
            _stage = InterfaceHelper.FindObject<IStage>();
			_cylinderTrackeable = FindObjectOfType<ARPlayerTrackableEventHandler>();

            Button shareButton = this.FindAndResolveComponent<Button>("Share<Button>", DisplayObject);
            shareButton.onClick.AddListener(delegate {
              
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });
               
            
        }

        protected override void ProcessUpdate()
        {

            
		
            if (_cylinderTrackeable == null || _stage == null)
                return;

			if (_cylinderTrackeable.IsTrackableVisible && ARLayoutWidget.Instance.isRunning)
                ARLayoutWidget.Instance.Terminate();
			else if (!_cylinderTrackeable.IsTrackableVisible && !ARLayoutWidget.Instance.isRunning)
                ARLayoutWidget.Instance.Initialize();

            IGame game = InterfaceHelper.FindObject<IGame>();

            if (game == null || !game.isRunning)
            {
				if (_cylinderTrackeable.IsTrackableVisible && !_stage.isVisible)
                    _stage.DisplayStage();
				else if (!_cylinderTrackeable.IsTrackableVisible && _stage.isVisible) 
                    _stage.HideStage();
                    
            }
            else
            {
                //Pause if trackable lost
				if (_cylinderTrackeable.IsTrackableVisible && game.isPaused)
                    game.UnPauseGame();
				else if (!_cylinderTrackeable.IsTrackableVisible && !game.isPaused)
                    game.PauseGame();
            }


                    }

        protected override void ProcessTermination()
        {
            Debug.Log("ARScreen Terminated");
        }



        public void OnVuforiaStarted() {

            _vuforiaStarted = true;

            SetAutoFocus(true);

        }

        public void OnVuforiaPaused(bool paused)
        {

            if (_vuforiaStarted)
            {
                SetAutoFocus(true);
            }

        }

        private void SetAutoFocus(bool autofocus) {

            Text cameraDebug = null;

            //Try to get a Debug Game Object
            try
            {
                cameraDebug = this.FindAndResolveComponent<Text>("CameraMode<Text>", DisplayObject);
            }
            catch (NullReferenceException e) {

                Debug.Log("No FocusMode DebugObject found. Focus Mode State debug won't be displayed on device's display.");

            }

            if (autofocus)
            {
                // Try to set vuforia auto focus, to improve samsung augmented reality
                bool focusModeSet = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
                
                if (!focusModeSet)
                    Debug.Log("Failed to set focus mode (unsupported mode).");
                
                if (cameraDebug != null)
                    cameraDebug.text = "CAMERA MODE SET: " + (focusModeSet ? "AUTOFOCUS" : "FAILED");

            }
            else {

                // Try to set vuforia auto focus, to improve samsung augmented reality
                bool focusModeSet = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);

                if (!focusModeSet)
                    Debug.Log("Failed to set focus mode (unsupported mode).");

                if (cameraDebug != null)
                    cameraDebug.text = "CAMERA MODE SET: " + (focusModeSet ? "NORMAL" : "FAILED");

            }

        }


    }

}
