﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

using ARMediaCoca.Views.Main.Widgets;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens {

    public class LoginScreen : UIModule<LoginScreen> {

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_LOGINSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.MIDDLE;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(UIModule<LoginScreen>.Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            Debug.Log("Login Screen Initialized");

            Button initButton = this.FindAndResolveComponent<Button>("Init<Button>", this.DisplayObject);

            initButton.onClick.AddListener(delegate {
                TutorialScreen.Instance.Initialize();
                Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button registerButton = this.FindAndResolveComponent<Button>("Register<Button>", DisplayObject);
            registerButton.onClick.AddListener(delegate {
                RegisterWidget.Instance.Initialize();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

        }

        protected override void ProcessUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) {
#if UNITY_ANDROID
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
#else
                        Application.Quit();
#endif
            }
        }

        protected override void ProcessTermination()
        {
            Debug.Log("Login Screen Terminated");
        }

    }

}