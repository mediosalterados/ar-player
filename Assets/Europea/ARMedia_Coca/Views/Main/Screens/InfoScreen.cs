﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;


using System;

using ARMediaCoca.Models.Interfaces;
using ARMediaCoca.Controllers.Services;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens {

    public class InfoScreen : UIModule<InfoScreen>, IScreen
    {

        Image _playImage;

        public bool isVideoPlaying {
            get { return _videoPlaying; }
        }
        bool _videoPlaying;

        Image _videoLoadThumb;

        MediaPlayerCtrl _videoPlayer;
       

        Button _fullScreenBtn;
        float _maxTimeToHideFullScreenBtn = 5.0f;
        float _timeToHideFullScreenBtn;

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_INFOSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {

                GameObject parent = GameObject.Find("3DCanvas");

                if (parent)
                    return parent.transform;
                else
                    return null;

            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            _videoPlaying = false;

            DisplayObject.transform.localRotation = Quaternion.identity;
            DisplayObject.transform.localPosition = new Vector3();

            Debug.Log("Info Screen Initialized");

            _playImage = this.FindAndResolveComponent<Image>("PlayIcon<Image>", DisplayObject);
            _videoPlayer = this.FindAndResolveComponent<MediaPlayerCtrl>("VideoFeed<RawImage>", DisplayObject);

            _videoLoadThumb = this.FindAndResolveComponent<Image>("VideoThumb<Image>", DisplayObject);

            Button videoButton = this.FindAndResolveComponent<Button>("VideoPlay<Button>", DisplayObject);
            videoButton.onClick.AddListener(delegate {
                ToogleVideoPlay();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            _fullScreenBtn = this.FindAndResolveComponent<Button>("FullScreen<Button>", DisplayObject);
            _fullScreenBtn.onClick.AddListener(delegate
            {
                GameObject videoFeed = this.Find("VideoFeed<RawImage>", DisplayObject);
                FullScreenWidget.Instance.videoFeed = videoFeed;
                FullScreenWidget.Instance.Initialize();
            });

            Button vid1Button = this.FindAndResolveComponent<Button>("Video1<Button>", DisplayObject);
            vid1Button.onClick.AddListener(delegate {
                PlayVideo(ResourcesPaths.VIDEO_1);
                _videoLoadThumb.sprite = AtlasManager.getSprite("coke_vid1_thumb");
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button vid2Button = this.FindAndResolveComponent<Button>("Video2<Button>", DisplayObject);
            vid2Button.onClick.AddListener(delegate {
                PlayVideo(ResourcesPaths.VIDEO_2);
                _videoLoadThumb.sprite = AtlasManager.getSprite("coke_vid2_thumb");
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button vid3Button = this.FindAndResolveComponent<Button>("Video3<Button>", DisplayObject);
            vid3Button.onClick.AddListener(delegate {
                PlayVideo(ResourcesPaths.VIDEO_3);
                _videoLoadThumb.sprite = AtlasManager.getSprite("coke_vid3_thumb");
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
            closeButton.onClick.AddListener(delegate {
                ScreenManager.Instance.CloseScreen();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            _fullScreenBtn.image.enabled = false;

        }

        protected override void ProcessUpdate()
        {
            if (_videoPlayer != null) {

                if (_videoPlaying)
                {
                    if (_videoPlayer.GetCurrentState() != MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
                        _videoPlayer.Play();

                    if (_fullScreenBtn.image.enabled && _timeToHideFullScreenBtn < 0)
                        _fullScreenBtn.image.enabled = false;
                    else
                        _timeToHideFullScreenBtn -= Time.deltaTime;
                }
                else {

                    if (_fullScreenBtn.image.enabled)
                        _fullScreenBtn.image.enabled = false;

                }
                       
            }

        }

        protected override void ProcessTermination()
        {
            Debug.Log("Info Screen Terminated");
            if (!FullScreenWidget.Instance.isRunning)
            {
                _videoPlayer.Stop();
                _videoPlayer.UnLoad();
            }
            _videoPlaying = false;
        }

        public bool isScreenRunning
        {
            get
            {
                return isRunning;
            }
        }

        public void StartScreen()
        {
            Initialize();
        }

        public void EndScreen(bool force = false)
        {
            Terminate(force);
        }

        public void ToogleVideoPlay(bool forcePlay = false) {


            if (_videoPlayer == null)
                return;


            if (!_videoPlaying || forcePlay)
            {
                _videoPlayer.Play();

                if (_playImage != null)
                    _playImage.enabled = false;

                if (_videoLoadThumb != null)
                    _videoLoadThumb.enabled = false;

                if (_fullScreenBtn != null) {
                    _fullScreenBtn.image.enabled = true;
                    _timeToHideFullScreenBtn = _maxTimeToHideFullScreenBtn;
                }

                _videoPlaying = true;
            }
            else {
                _videoPlayer.Pause();

                if (_playImage != null)
                    _playImage.enabled = true;

                if (_videoLoadThumb != null)
                    _videoLoadThumb.enabled = true;

                _videoPlaying = false;
            }


        }

        public void PlayVideo(string path) {

            if (_videoPlayer == null)
                return;
                       

            _videoPlayer.Load(path);

            if (_playImage != null)
                _playImage.enabled = true;

            if (_videoLoadThumb != null)
                _videoLoadThumb.enabled = true;

            _videoPlaying = false;
                      

        }

    }

}
