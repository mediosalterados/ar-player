﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

using ARMediaCoca.Views.Main.Screens;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Widgets
{

    public class RegisterWidget : UIModule<RegisterWidget>
    {

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_REGISTERWIDGET;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                if (LoginScreen.Instance.isRunning)
                    return LoginScreen.Instance.DisplayObject.transform;
                else
                    return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            Button logIn = this.FindAndResolveComponent<Button>("Login<Button>", DisplayObject);
            logIn.onClick.AddListener(delegate {
                Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });
        }

        protected override void ProcessUpdate()
        {

        }

        protected override void ProcessTermination()
        {

        }

    }

}
