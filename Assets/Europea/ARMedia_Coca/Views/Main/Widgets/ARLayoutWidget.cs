﻿using UnityEngine;
using System.Collections;

using GLIB.Interface;
using System;

using ARMediaCoca.Views.Main.Screens;

namespace ARMediaCoca.Views.Main.Widgets { 

    public class ARLayoutWidget : UIModule<ARLayoutWidget> {

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_ARLAYOUTWIDGET;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                if (ARScreen.Instance.isRunning)
                    return ARScreen.Instance.DisplayObject.transform;
                else
                    return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.MIDDLE;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            
        }

        protected override void ProcessUpdate()
        {
            
        }

        protected override void ProcessTermination()
        {
            
        }

    }

}
