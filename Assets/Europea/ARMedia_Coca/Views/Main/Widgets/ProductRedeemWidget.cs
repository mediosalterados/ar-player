﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Extended;
using GLIB.Interface;
using GLIB.Audio;

using System;

using ARMediaCoca.Views.Main.Screens;

namespace ARMediaCoca.Views.Main.Widgets
{

    public class ProductRedeemWidget : UIModule<ProductRedeemWidget>
    {

        protected override Transform DisplayObjectParent
        {
            get
            {
                if (ARScreen.Instance.isRunning)
                    return ARScreen.Instance.DisplayObject.transform;
                else
                    return null;
            }
        }

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_PRODUCTREDEEMWIDGET;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", this.DisplayObject);
            closeButton.onClick.AddListener(delegate { Terminate(); SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); });
        }

        protected override void ProcessUpdate()
        {
            
        }

        protected override void ProcessTermination()
        {
            
        }

    }

}
