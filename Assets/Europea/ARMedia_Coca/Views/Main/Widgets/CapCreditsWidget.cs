﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using GLIB.Utils;

using System;

using ARMediaCoca.Models.Interfaces;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Widgets { 

    public class CapCreditsWidget : UIModule<CapCreditsWidget>, IScreen {

        InputField _capCreditField;

        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_CAPCREDITSWIDGET;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {
                return null;
            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }


        protected override void ProcessInitialization()
        {
            _capCreditField = this.FindAndResolveComponent<InputField>("CapCode<InputField>", DisplayObject);

            Button cancel = this.FindAndResolveComponent<Button>("Cancel<Button>", this.DisplayObject);
            cancel.onClick.AddListener(delegate {
                Terminate();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Button proceed = this.FindAndResolveComponent<Button>("Next<Button>", DisplayObject);
            proceed.onClick.AddListener(delegate {

                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);

                if (_capCreditField.text != "")
                {
                    User.USER_POINTS += 10;
                    Terminate();
                    NotificationSystem.Instance.NotifyMessage(ResourcesPaths.CREDITS_ADD_SUCESS, null, delegate { SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); });
                }
                else
                    NotificationSystem.Instance.NotifyMessage(ResourcesPaths.CREDITS_ADD_FAILURE, null, delegate { SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK); });
            }
            );
        }

        protected override void ProcessUpdate()
        {
            
        }

        protected override void ProcessTermination()
        {
            
        }

        public bool isScreenRunning
        {
            get { return isRunning; }
        }

        public void StartScreen()
        {
            Initialize();
        }

        public void EndScreen(bool force = false)
        {
            Terminate();
        }


    }

}
