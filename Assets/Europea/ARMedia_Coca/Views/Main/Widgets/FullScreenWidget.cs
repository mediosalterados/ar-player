﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GLIB.Interface;
using GLIB.Extended;
using System;

using ARMediaCoca.Models.Interfaces;

public class FullScreenWidget : UIModule<FullScreenWidget> {

    ScreenOrientation _previousScreenOrientation;

    public GameObject videoFeed {
        get {
            return _videoFeed;
        }

        set {

            //First unset fullscreen videofeed (if any)
            if(_isVideoFeedSetInContainer)
                SetVideoToFullScreen(false);

            RectTransform checkRectTransform = value.GetComponent<RectTransform>();

            if (checkRectTransform != null) {

                _videoFeed = value;

                _videoFeedPrevRectTransform = new RectTransformMemento();
                _videoFeedPrevRectTransform.anchoredPosition3D = checkRectTransform.anchoredPosition3D;
                _videoFeedPrevRectTransform.sizeDelta = checkRectTransform.sizeDelta;
                _videoFeedPrevRectTransform.anchorMax = checkRectTransform.anchorMax;
                _videoFeedPrevRectTransform.anchorMin = checkRectTransform.anchorMin;
                _videoFeedPrevRectTransform.localRotation = checkRectTransform.localRotation;
                _videoFeedPrevRectTransform.localScale = checkRectTransform.localScale;
                _videoFeedPrevRectTransform.pivot = checkRectTransform.pivot;

                _videoFeedPrevParent = _videoFeed.transform.parent;

                _videoFeedChildIndex = _videoFeed.transform.GetSiblingIndex();
                
                // If its running just set the videoFeed into container right now.
                if (isRunning)
                    SetVideoToFullScreen(true);

            }
            else {
                Debug.LogWarning("No RectTransform component found on the videoFeed GameObject, not doing anything.");
            }
                       
        }
    }    
    GameObject _videoFeed;
    RectTransformMemento _videoFeedPrevRectTransform;
    Transform _videoFeedPrevParent;
    int _videoFeedChildIndex;

    struct RectTransformMemento {

        public Vector3 anchoredPosition3D;
        public Vector2 sizeDelta;
        public Vector2 anchorMax;
        public Vector2 anchorMin;
        public Quaternion localRotation;
        public Vector3 localScale;
        public Vector2 pivot;

    }

    GameObject _container;

    bool _isVideoFeedSetInContainer {

        get {
            if (_container != null)
            {
              
                if (_container.transform.childCount > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

    }
    
    protected override Transform DisplayObjectParent
    {
        get
        {
            return null;
        }
    }

    protected override string DisplayObjectPath
    {
        get
        {
            return ResourcesPaths.TEMPLATE_MAIN_FULLSCREENVIDEOWIDGET;
        }
    }

    protected override Vector2? DisplayObjectPosition
    {
        get
        {
            return null;
        }
    }

    protected override int DisplayObjectZIndex
    {
        get
        {
            return (int)ZIndexPlacement.TOP;
        }
    }

    protected override Transition InOutTransition
    {
        get
        {
            return new Transition(Transition.InOutAnimations.SCALE);
        }
    }

    protected override void ProcessInitialization()
    {
        //Get current screen orientation
        _previousScreenOrientation = Screen.orientation;

        Screen.orientation = ScreenOrientation.LandscapeRight;

        _container = this.Find("Container", DisplayObject);

        Button closeBtn = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
        closeBtn.onClick.AddListener(delegate { Terminate(); });

        SetVideoToFullScreen(true);

        
    }

    protected override void ProcessUpdate()
    {
        

    }

    protected override void ProcessTermination()
    {
        Screen.orientation = _previousScreenOrientation;

        if (_isVideoFeedSetInContainer)
            SetVideoToFullScreen(false);

        //Try stop all media players if parent has been destroyed
        if (_videoFeedPrevParent == null)
        {
            MediaPlayerCtrl[] mediaplayers = GameObject.FindObjectsOfType<MediaPlayerCtrl>();

            foreach (MediaPlayerCtrl player in mediaplayers)
            {

                player.Stop();
                player.UnLoad();

            }
        }
    }

    void SetVideoToFullScreen(bool set) {


        if (_videoFeed == null)
            return;

        if (set) {

            if (_isVideoFeedSetInContainer)
                return;

            _videoFeed.transform.SetParent(_container.transform);

            RectTransform videoFeedRectTransform = _videoFeed.GetComponent<RectTransform>();

            

            /*
                _videoFeedPrevRectTransform = new RectTransform();
                _videoFeedPrevRectTransform.anchoredPosition3D = checkRectTransform.anchoredPosition3D;
                _videoFeedPrevRectTransform.sizeDelta = checkRectTransform.sizeDelta;
                _videoFeedPrevRectTransform.anchorMax = checkRectTransform.anchorMax;
                _videoFeedPrevRectTransform.anchorMin = checkRectTransform.anchorMin;
                _videoFeedPrevRectTransform.localRotation = checkRectTransform.localRotation;
                _videoFeedPrevRectTransform.localScale = checkRectTransform.localScale;
                _videoFeedPrevRectTransform.pivot = checkRectTransform.pivot;
            */

            videoFeedRectTransform.anchoredPosition3D = new Vector3();
            videoFeedRectTransform.sizeDelta = new Vector2(3413, 1920);
            videoFeedRectTransform.anchorMax = new Vector2(0.5f, 0.5f);
            videoFeedRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            videoFeedRectTransform.localRotation = Quaternion.identity;
			videoFeedRectTransform.localScale = new Vector3(-0.3f,-0.3f,-0.3f);
            videoFeedRectTransform.pivot = new Vector2(0.5f, 0.5f);

            Debug.Log("Set to full screen");
            
        }
        else {

            // If parent has been destroyed then don't unset it at all.
            if (_videoFeedPrevParent == null || !_videoFeedPrevParent.gameObject.activeSelf)
                return;

            _videoFeed.transform.SetParent(_videoFeedPrevParent);

            RectTransform videoFeedRectTransform = _videoFeed.GetComponent<RectTransform>();

            _videoFeed.transform.SetSiblingIndex(_videoFeedChildIndex);

            videoFeedRectTransform.anchoredPosition3D = _videoFeedPrevRectTransform.anchoredPosition3D;
            videoFeedRectTransform.sizeDelta = _videoFeedPrevRectTransform.sizeDelta;
            videoFeedRectTransform.anchorMax = _videoFeedPrevRectTransform.anchorMax;
            videoFeedRectTransform.anchorMin = _videoFeedPrevRectTransform.anchorMin;
            videoFeedRectTransform.localRotation = _videoFeedPrevRectTransform.localRotation;
            videoFeedRectTransform.localScale = _videoFeedPrevRectTransform.localScale;
            videoFeedRectTransform.pivot = _videoFeedPrevRectTransform.pivot;

            // Fix alpha 0 RawImage Problem
            RawImage rawImg = _videoFeed.GetComponent<RawImage>();

            if (rawImg != null) {

                Color rawImageColor = rawImg.color;
                rawImageColor.a = 1.0f;

                rawImg.color = rawImageColor;
            }

            Debug.Log("Unset to full screen");

        }

    }

}
