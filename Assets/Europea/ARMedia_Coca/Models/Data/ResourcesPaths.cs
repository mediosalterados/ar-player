﻿using UnityEngine;
using System.Collections;

public sealed class ResourcesPaths {

    private ResourcesPaths() { }

    public const float VOLUME_SFX_UI_CLICK = 0.6f;

    public const string OBJECT_WATERRINGS_GAMESTAGE = "Objects/GameStage";

    public const string PREFAB_RING = "Objects/Ring2";
    public const string PREFAB_PARTICLES_TIMERINGTRAIL = "Particles/TimeRingTrail";
    public const string PREFAB_PARTICLES_BADRINGTRAIL = "Particles/BadRingTrail";
    public const string PREFAB_PARTICLES_NORMALRINGTRAIL = "Particles/NormalRingTrail";
    public const string PREFAB_PARTICLES_RINGHIT = "Particles/RingHit";
    public const string PREFAB_PARTICLES_RINGEXPLOSION = "Particles/RingExplosion";
    public const string PREFAB_PARTICLES_CAPFIREWORKS = "Particles/CapFireworks";
    public const string PREFAB_PARTICLES_CAPHIT = "Particles/CapHit";

    public const string MAT_TIMERING = "Materials/TimeRing";
    public const string MAT_BADRING = "Materials/BadRing";
    public const string MAT_NORMALRING = "Materials/NormalRing";

    public const string SFX_WATER_PUSH = "Audio/waterPush";
    public const string SFX_GLASS_HIT = "Audio/glassHit";
    public const string SFX_BOTTLE_HIT = "Audio/bottleHit";
    public const string SFX_RING_DESTROY = "Audio/ringDestroy";
    public const string SFX_SCORE_UP = "Audio/scoreUp";
    public const string SFX_SCORE_DOWN = "Audio/scoreDown";
    public const string SFX_TIME_BONUS = "Audio/timeBonus";
    public const string SFX_GAME_RESULTS = "Audio/gameResults";
    public const string SFX_BOTTLE_POP = "Audio/bottlePop";
    public const string SFX_HIT_YES_SHORT = "Audio/hitYes";
    public const string SFX_HIT_YES = "Audio/hitYes2";
    public const string SFX_HIT_NO = "Audio/hitNo";
    public const string SFX_MENU_DISP_TICK = "Audio/menuDisplaceTick";
    public const string SFX_RESULTS_GOOD = "Audio/resultsGood";
    public const string SFX_RESULTS_BAD = "Audio/resultsBad";
    public const string SFX_RESULTS_LAUGH = "Audio/resultsLaugh";
    public const string SFX_UI_CLICK = "Audio/uiClick";

    public const string TEMPLATE_MAIN_ARSCREEN = "Templates/Main/Screens/ARScreen";
    public const string TEMPLATE_MAIN_ACCOUNTSCREEN = "Templates/Main/Screens/AccountScreen";
    public const string TEMPLATE_MAIN_GAMESSCREEN = "Templates/Main/Screens/GamesScreen";
	public const string TEMPLATE_MAIN_VIDEOSCREEN = "Templates/Main/Screens/VideoScreen";
    public const string TEMPLATE_MAIN_INFOSCREEN = "Templates/Main/Screens/InfoScreen";
    public const string TEMPLATE_MAIN_LOGINSCREEN = "Templates/Main/Screens/LoginScreen";
    public const string TEMPLATE_MAIN_TUTORIALSCREEN = "Templates/Main/Screens/TutorialScreen";
    public const string TEMPLATE_MAIN_CONTROLSSCREEN = "Templates/Main/Screens/ControlsScreen";
    public const string TEMPLATE_MAIN_SHOPSCREEN = "Templates/Main/Screens/ShopScreen";
    public const string TEMPLATE_MAIN_ARLAYOUTWIDGET = "Templates/Main/Widgets/ARLayoutWidget";
    public const string TEMPLATE_MAIN_CAPCREDITSWIDGET = "Templates/Main/Widgets/CapCreditsWidget";
    public const string TEMPLATE_MAIN_REGISTERWIDGET = "Templates/Main/Widgets/RegisterWidget";
    public const string TEMPLATE_MAIN_PRODUCTREDEEMWIDGET = "Templates/Main/Widgets/ProductRedeemWidget";
    public const string TEMPLATE_MAIN_FULLSCREENVIDEOWIDGET = "Templates/Main/Widgets/FullScreenVideoWidget";

    public const string TEMPLATE_AR_WRG_SCREEN = "Templates/AR/WaterRingsGame/Screens/WaterRingsScreen";
    public const string TEMPLATE_AR_WRG_WIDGET_HUD = "Templates/AR/WaterRingsGame/Widgets/WaterRingsHUD";
    public const string TEMPLATE_AR_WRG_WIDGET_INSTRUCTIONS = "Templates/AR/WaterRingsGame/Widgets/WaterRingsInstructions";
    public const string TEMPLATE_AR_WRG_WIDGET_RESULTS = "Templates/AR/WaterRingsGame/Widgets/WaterRingsResults";

    public const string TEMPLATE_GRIMOIRE_NOTIFICATIONSYSTEM = "Templates/Grimoire/NotificationSystem";

    public const string SPRITE_ATLAS = "Sprites/armedia_coca_atlas";

    public const string SPRITE_NAME_SODA_H = "b1_h";
    public const string SPRITE_NAME_BALLON_H = "b2_h";
    public const string SPRITE_NAME_COVER_H = "b3_h";
    public const string SPRITE_NAME_USB_H = "b4_h";
    public const string SPRITE_NAME_RADIO_H = "b5_h";
    public const string SPRITE_NAME_TICKET_H = "b6_h";

    public const string SPRITE_NAME_SODA_LARGE = "p_coke";
    public const string SPRITE_NAME_BALLON_LARGE = "p_balloon";
    public const string SPRITE_NAME_COVER_LARGE = "p_phone_cover";
    public const string SPRITE_NAME_USB_LARGE = "p_usb";
    public const string SPRITE_NAME_RADIO_LARGE = "p_radio";
    public const string SPRITE_NAME_TICKET_LARGE = "p_ticket";

    public const string BGM_1 = "Audio/BGM1";

    public const string VIDEO_1 = "coke_vid1.mp4";
    public const string VIDEO_2 = "coke_vid2.mp4";
    public const string VIDEO_3 = "coke_vid3.mp4";

    public const string POINTS_REDEEM_SUCESS = "¡Felicidades!\n\nHas adquirido tu producto, revisa tu correo electrónico para continuar con el proceso.\n";
    public const string POINTS_REDEEM_FAILURE = "¡Ay yai yai!\n\nNo tienes puntos suficientes.\n";
    public const string POINTS_SLOT_GAME_LOCKED = "¡Oops!\n\nNecesitas 2,000 puntos para desbloquear éste juego.\n";
    public const string POINTS_ASTRO_GAME_LOCKED = "¡Oops!\n\nNecesitas 5,000 puntos para desbloquear éste juego.\n";
    public const string POINTS_REDEEM_PROMPTA = "¿Deseas gastar ";
    public const string POINTS_REDEEM_PROMPTB = " de tus puntos?\n";

    public const string CREDITS_ADD_SUCESS = "¡Felicidaes!\n\nHas añadido 10 puntos a tu cuenta exitosamente.\n";
    public const string CREDITS_ADD_FAILURE = "¡Oops!\n\nCódigo no válido, ¿tan siquiera ingresaste algún código?.\n";
    public const string CREDITS_PLAY_FAILURE = "¡Ay yai yai!\n\nNo tienes créditos suficientes.\n";

}
