﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is just a fake user class, do not use this for real implementation!
/// </summary>
public sealed class User {

    public static string USER_NAME = "Julio Zamora";
    public static int USER_CREDITS = 0;
    public static int USER_POINTS = 1000;

}
